<?php
// Plugin definition
$plugin = array(
  'title' => t('Hydrogen Flipped'),
  'icon' => 'hydrogen_flipped.png',
  'category' => t('Signage'),
  'theme' => 'hydrogen_flipped',
  'css' => 'hydrogen_flipped.css',
  'regions' => array(
    'sidebar' => t('Content Sidebar'),
    'contentmain' => t('Content'),
    'headerfirst' => t('Header First'),
    'headersecond' => t('Header Second'),
    'footer' => t('Footer'),
  ),
);
