<?php
/**
 * @file
 * Nitrogen layout.
 * Inspired by the Panopoly Moscone Flipped template.
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>

<div class="panel-display nitrogen clearfix <?php !empty($class) ? print $class : ''; ?>" <?php !empty($css_id) ? print "id=\"$css_id\"" : ''; ?>>

  <div class="nitrogen-container nitrogen-header periodic-header clearfix">
    <div class="nitrogen-headerfirst periodic-headerfirst panel-panel">
      <div class="nitrogen-headerfirst-inner nitrogen-container-inner periodic-headerfirst-inner panel-panel-inner">
        <?php print $content['headerfirst']; ?>
      </div>
    </div>
    <div class="nitrogen-container nitrogen-headersecond periodic-headersecond panel-panel">
      <div class="nitrogen-headersecond-inner nitrogen-container-inner periodic-headersecond-inner panel-panel-inner">
        <?php print $content['headersecond']; ?>
      </div>
    </div>
  </div>

  <div class='nitrogen-container nitrogen-content periodic-content clearfix'>
    <div class="nitrogen-container periodic-container nitrogen-contentmain periodic-contentmain clearfix panel-panel">
      <div class="nitrogen-contentmain-inner nitrogen-container-inner periodic-contentmain-inner panel-panel-inner">
        <?php print $content['contentmain']; ?>
      </div>
    </div>
  </div>

</div><!-- /.nitrogen -->
