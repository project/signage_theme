<?php
// Plugin definition
$plugin = array(
  'title' => t('Nitrogen'),
  'icon' => 'nitrogen.png',
  'category' => t('Signage'),
  'theme' => 'nitrogen',
  'css' => 'nitrogen.css',
  'regions' => array(
    'contentmain' => t('Content'),
    'headerfirst' => t('Header First'),
    'headersecond' => t('Header Second'),
  ),
);
