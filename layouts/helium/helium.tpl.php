<?php
/**
 * @file
 * Hydrogen layout.
 * Inspired by the Panopoly Moscone Flipped template.
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>

<div class="panel-display helium clearfix <?php !empty($class) ? print $class : ''; ?>" <?php !empty($css_id) ? print "id=\"$css_id\"" : ''; ?>>

  <div class="helium-container helium-header periodic-header clearfix">
    <div class="helium-headerfirst periodic-headerfirst panel-panel">
      <div class="helium-headerfirst-inner helium-container-inner periodic-headerfirst-inner panel-panel-inner">
        <?php print $content['headerfirst']; ?>
      </div>
    </div>
    <div class="helium-container helium-headersecond periodic-headersecond panel-panel">
      <div class="helium-headersecond-inner helium-container-inner periodic-headersecond-inner panel-panel-inner">
        <?php print $content['headersecond']; ?>
      </div>
    </div>
  </div>

  <div class="helium-container helium-content periodic-content clearfix">
    <div class="helium-container periodic-container helium-contentmain periodic-contentmain clearfix panel-panel">
      <div class="helium-contentmain-inner helium-container-inner periodic-contentmain-inner panel-panel-inner">
        <?php print $content['contentmain']; ?>
      </div>
    </div>
    <div class="helium-container periodic-container helium-sidebar periodic-sidebar clearfix panel-panel">
      <div class="helium-sidebar-inner helium-container-inner periodic-sidebar-inner panel-panel-inner">
        <?php print $content['sidebar']; ?>
      </div>
    </div>
  </div>

</div><!-- /.helium -->

