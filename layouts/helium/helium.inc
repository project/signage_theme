<?php
// Plugin definition
$plugin = array(
  'title' => t('Helium'),
  'icon' => 'helium.png',
  'category' => t('Signage'),
  'theme' => 'helium',
  'css' => 'helium.css',
  'regions' => array(
    'sidebar' => t('Content Sidebar'),
    'contentmain' => t('Content'),
    'headerfirst' => t('Header First'),
    'headersecond' => t('Header Second'),
  ),
);
