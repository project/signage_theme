<?php
// Plugin definition
$plugin = array(
  'title' => t('Hydrogen'),
  'icon' => 'hydrogen.png',
  'category' => t('Signage'),
  'theme' => 'hydrogen',
  'css' => 'hydrogen.css',
  'regions' => array(
    'sidebar' => t('Content Sidebar'),
    'contentmain' => t('Content'),
    'headerfirst' => t('Header First'),
    'headersecond' => t('Header Second'),
    'footer' => t('Footer'),
  ),
);
