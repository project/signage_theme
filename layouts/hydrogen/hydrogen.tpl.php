<?php
/**
 * @file
 * Hydrogen layout.
 * Inspired by the Panopoly Moscone Flipped template.
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>

<div class="panel-display hydrogen flex-container clearfix <?php !empty($class) ? print $class : ''; ?>" <?php !empty($css_id) ? print "id=\"$css_id\"" : ''; ?>>
  <div class="hydrogen-container hydrogen-header periodic-header clearfix">
    <div class="hydrogen-headerfirst periodic-headerfirst panel-panel">
      <div class="hydrogen-headerfirst-inner hydrogen-container-inner periodic-headerfirst-inner panel-panel-inner">
      <?php print $content['headerfirst']; ?>
    </div>
  </div>
    <div class="hydrogen-container hydrogen-headersecond periodic-headersecond panel-panel">
      <div class="hydrogen-headersecond-inner hydrogen-container-inner periodic-headersecond-inner panel-panel-inner">
        <?php print $content['headersecond']; ?>
      </div>
    </div>
  </div>

  <div class="hydrogen-container hydrogen-content periodic-content clearfix">
    <div class="hydrogen-container periodic-container hydrogen-contentmain periodic-contentmain clearfix panel-panel">
      <div class="hydrogen-contentmain-inner hydrogen-container-inner periodic-contentmain-inner panel-panel-inner">
      <?php print $content['contentmain']; ?>
      </div>
    </div>
    <div class="hydrogen-container periodic-container hydrogen-sidebar periodic-sidebar clearfix panel-panel">
      <div class="hydrogen-sidebar-inner hydrogen-container-inner periodic-sidebar-inner panel-panel-inner">
      <?php print $content['sidebar']; ?>
      </div>
    </div>
  </div>

  <div class="hydrogen-container hydrogen-footer periodic-footer clearfix">
    <div class="hydrogen-container periodic-container hydrogen-footer periodic-footer clearfix panel-panel">
      <div class="hydrogen-footer-inner hydrogen-container-inner periodic-footer-inner panel-panel-inner">
      <?php print $content['footer']; ?>
      </div>
    </div>
</div>

</div><!-- /.hydrogen -->
