<?php
/**
 * @file
 * Oxygen layout.
 * Inspired by the Panopoly Moscone Flipped template.
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>

<div class="panel-display oxygen flex-container clearfix <?php !empty($class) ? print $class : ''; ?>" <?php !empty($css_id) ? print "id=\"$css_id\"" : ''; ?>>

  <div class="oxygen-container oxygen-header periodic-header clearfix">
    <div class="oxygen-headerfirst periodic-headerfirst panel-panel">
      <div class="oxygen-headerfirst-inner oxygen-container-inner periodic-headerfirst-inner panel-panel-inner">
      <?php print $content['headerfirst']; ?>
    </div>
  </div>
    <div class="oxygen-container oxygen-headersecond periodic-headersecond panel-panel">
      <div class="oxygen-headersecond-inner oxygen-container-inner periodic-headersecond-inner panel-panel-inner">
        <?php print $content['headersecond']; ?>
      </div>
    </div>
  </div>


  <div class=" oxygen-container oxygen-content periodic-content clearfix">
    <div class="oxygen-container periodic-container oxygen-contentmain periodic-contentmain clearfix panel-panel">
      <div class="oxygen-contentmain-inner oxygen-container-inner periodic-contentmain-inner panel-panel-inner">
        <?php print $content['contentmain']; ?>
     </div>
    </div>
  </div>

  <div class="oxygen-container oxygen-footer periodic-footer clearfix">
    <div class="oxygen-container periodic-container oxygen-footer periodic-footer clearfix panel-panel">
      <div class="oxygen-footer-inner oxygen-container-inner periodic-footer-inner panel-panel-inner">
      <?php print $content['footer']; ?>
      </div>
    </div>
  </div>

</div><!-- /.oxygen -->
