<?php
// Plugin definition
$plugin = array(
  'title' => t('Oxygen'),
  'icon' => 'oxygen.png',
  'category' => t('Signage'),
  'theme' => 'oxygen',
  'css' => 'oxygen.css',
  'regions' => array(
    'contentmain' => t('Content'),
    'headerfirst' => t('Header First'),
    'headersecond' => t('Header Second'),
    'footer' => t('Footer'),
  ),
);
