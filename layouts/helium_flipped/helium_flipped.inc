<?php
// Plugin definition
$plugin = array(
  'title' => t('Helium Flipped'),
  'icon' => 'helium_flipped.png',
  'category' => t('Signage'),
  'theme' => 'helium_flipped',
  'css' => 'helium_flipped.css',
  'regions' => array(
    'sidebar' => t('Content Sidebar'),
    'contentmain' => t('Content'),
    'headerfirst' => t('Header First'),
    'headersecond' => t('Header Second'),
  ),
);
