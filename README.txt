Signage Theme is built off of the Boilerplate theme.
(http://drupal.org/project/boilerplate).

__________________________________________________________________________________________

Installation

- Download Signage Theme from https://github.com/ITS-UofIowa/signage_theme
- Unpack the downloaded file and place the Boilerplate folder in your Drupal
  installation under one of the following locations:

    * sites/all/themes
    * sites/default/themes
    * sites/example.com/themes

- If you're using the Signage install profile, a Drush make file is included
  which will place the theme in profiles/signage/themes

- Log in as an administrator on your Drupal site and go to
  Administer > Site building > Themes (admin/build/themes) and make Signage
  Theme the default theme.

Extending

- Just like other Drupal 7 themes, you are free to create a subtheme of the
  Signage Theme for your own styles.
